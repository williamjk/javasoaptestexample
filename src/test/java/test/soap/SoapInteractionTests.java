package test.soap;

import java.io.InputStream;
import java.io.StringWriter;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;

/*
 * ALLOW SSL KEY (stemac.cer)
 * C:\dev\SDKs\jdk1.6.0_45\bin> keytool -import -file c:\dev\stemac.cer -keystore cacerts
 */

public class SoapInteractionTests {

	// reference variables
	private SOAPConnection soapConnection;
	private SOAPMessage soapMessage;
	private SOAPPart soapPart;
	public String solicitacao = "470";
	public String linkbpm;

	@Before
	public void setUp() throws SOAPException {
		SOAPConnectionFactory soapConnectionFactory;
		MessageFactory messageFactory;

		try {
			// object initialization
			soapConnectionFactory = SOAPConnectionFactory.newInstance();
			soapConnection = soapConnectionFactory.createConnection();

			messageFactory = MessageFactory.newInstance();
			soapMessage = messageFactory.createMessage();

			soapPart = soapMessage.getSOAPPart();
			soapPart.getEnvelope();

		} catch (UnsupportedOperationException e) {
			System.out.println("ERROR: setUp: " + e.getMessage());
		} catch (SOAPException e) {
			System.out.println("ERROR: setUp: " + e.getMessage());
		} 

	}

	public String SolicitarAtividade() {
		try {
			setUp();

			InputStream fis = (InputStream) SoapInteractionTests.class
					.getResourceAsStream("SolicitarAtividade.xml");
			System.out.println(fis);
			String text = IOUtils.toString(fis);
			text = text.replaceAll("nuSolicitacao", solicitacao);
			System.out.println(" ");
			System.out.println("SolicitarAtividade" + text); // xml with replace
			System.out.println(" ");
			InputStream in = IOUtils.toInputStream(text, "UTF-8");

			// Object for message parts
			StreamSource prepMsg = new StreamSource(in);

			soapPart.setContent(prepMsg);

			// Save message
			soapMessage.saveChanges();

			// Send request to url
			String endPoinUrl = "https://mtz-srv-bpm10.stemac.com.br:9443/teamworks/webservices/PSGC/Atividades.tws";

			doTrustToCertificates();
			SOAPMessage messagerequest = soapConnection.call(soapMessage,
					endPoinUrl);

			// response data
			Source source = messagerequest.getSOAPPart().getContent();

			// Create transformer object
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();

			// set response parameter to string
			StringWriter writer = new StringWriter();
			StreamResult sResult = new StreamResult(writer);
			transformer.transform(source, sResult);
			String result1 = writer.toString();
			System.out.println("XML resultado solicitar atividade: " + result1);
			System.out.println(" ");
			Assert.assertTrue(result1.contains("<sucesso>true</sucesso>"));

			// waiting build
			Thread.sleep(5000);
			linkbpm = RetornaLink();

		} catch (Exception e) {
			System.out.println("ERROR: SolicitarAtividade: " + e.getMessage());
		}
		return linkbpm;
	}


	public String RetornaLink() {
		String linkSE = ObterLinkAtividade();
		System.out.println(" ");
		System.out.println(linkSE);
		return linkSE;
	}

	public String ObterLinkAtividade() {
		String link = "";
		try {

			InputStream fis = (InputStream) SoapInteractionTests.class
					.getResourceAsStream("ObterLinkAtividade.xml");

			String text = IOUtils.toString(fis);
			text = text.replaceAll("nuSolicitacao", solicitacao);
			System.out.println("ObterLinkAtividade XML:" + text); // xml with
																	// replace
			InputStream in = IOUtils.toInputStream(text, "UTF-8");

			// Object for message parts
			StreamSource prepMsg = new StreamSource(in);

			soapPart.setContent(prepMsg);

			// Save message
			soapMessage.saveChanges();

			// Send request to url
			String endPoinUrl = "https://mtz-srv-bpm10.stemac.com.br:9443/teamworks/webservices/PSGC/Atividades.tws";

			doTrustToCertificates();
			SOAPMessage messagerequest = soapConnection.call(soapMessage,
					endPoinUrl);

			// response data
			Source source = messagerequest.getSOAPPart().getContent();

			// Create transformer object
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();

			// set response parameter to string
			StringWriter writer = new StringWriter();
			StreamResult sResult = new StreamResult(writer);
			transformer.transform(source, sResult);
			String result1 = writer.toString();
			int initialIndex = result1.indexOf("<linkAtividade>")
					+ "<linkAtividade>".length();
			int finalIndex = result1.indexOf("</linkAtividade>");
			link = result1.substring(initialIndex, finalIndex);
			System.out.println(" ");
			System.out.println("XML Resultado Obter Link: " + result1);
			Assert.assertTrue(result1
					.contains("<linkAtividade>http://mtz-srv-bpm10.stemac.com.br:9080/teamworks/process.lsw?zWorkflowState=1&amp;zTaskId="));

			// close connection
			soapConnection.close();

		} catch (Exception e) {
			System.out.println("ERROR: ObterLinkAtividade: " + e.getMessage());
		}

		// System.out.println(link);
		return link;
	}

	static public void doTrustToCertificates() throws Exception {
		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkServerTrusted(X509Certificate[] certs,
					String authType) throws CertificateException {
			}

			public void checkClientTrusted(X509Certificate[] certs,
					String authType) throws CertificateException {
			}
		} };

		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		HostnameVerifier hv = new HostnameVerifier() {
			public boolean verify(String urlHostName, SSLSession session) {
				if (!urlHostName.equalsIgnoreCase(session.getPeerHost())) {
					System.out.println("Warning: URL host '" + urlHostName
							+ "' is different to SSLSession host '"
							+ session.getPeerHost() + "'.");
				}
				return true;
			}
		};
		HttpsURLConnection.setDefaultHostnameVerifier(hv);
	}

}
