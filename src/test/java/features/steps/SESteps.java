package features.steps;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import unitSeleniumTest.ConfiguradorTest;
import unitSeleniumTest.DimensionadorTest_OK;
import utils.BPMUtils;
import utils.DriverTestInstance;
import utils.LibraryUtils;
import utils.SqlUtil;
import bpmn.SoapTest;
import cucumber.api.PendingException;
import cucumber.api.java.en.When;

public class SESteps extends DriverTestInstance {

	private SoapInteractionTests soap = PageFactory.initElements(driver, SoapInteractionTests.class);
	private String link;
	
	@When("^Start a SE$")
	public void iniciar_a_SE() throws Throwable {

		link = soap.SolicitarAtividade();
				driver.get(link);
		
		// login bpmn
		driver.findElement(By.id("username")).sendKeys("p000506");
		driver.findElement(By.id("password")).sendKeys("stemac");
		driver.findElement(By.id("log_in")).click();
	}

}
